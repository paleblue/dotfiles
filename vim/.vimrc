"""""""""""""
"  Prelude  "
"""""""""""""

set shell=bash
set t_u7= " don't ask for cursor position from xterm -- this can cause vim opening in Replace mode

syntax on

let mapleader=","

" if (empty($TMUX))
    if (has("nvim"))
      let $NVIM_TUI_ENABLE_TRUE_COLOR=1
    endif
    if (has("termguicolors"))
      set termguicolors
    endif
" endif

""""""""""
"  Sets  "
""""""""""
"" configure
set autochdir     " changes working directory to that of file
set re=0 " turn off old regexp engine
set hidden
set nobackup
set nowritebackup
set noshowmode    " don't need it, we have airline
if &compatible
  set nocompatible               " Be iMproved
endif
"" display
set number
set cindent
set ignorecase
set smartcase
set scrolloff=2
set visualbell
set noerrorbells
set cursorline
set encoding=utf-8 " Necessary to show Unicode glyphs
set nrformats=hex
"" tabs
set tabstop=4
set softtabstop=0
set shiftwidth=2
set expandtab
set smarttab
"" other 
set backspace=indent,eol,start
set cpoptions+=$
set splitright
set splitbelow
set hlsearch
set incsearch
set wildmenu
set showmatch
set wildignore=*.swp,*.bak,*.pyc,*.class
set pastetoggle=<F2>

" set pastetoggle=<F2>

"" cursor settings
" helps get cursor set right when starting
let &t_ti.="\e[1 q" " start termcap mode
let &t_SI.="\e[5 q" " start Insert mode
let &t_EI.="\e[1 q" " end Insert mode
let &t_te.="\e[0 q" " end termcap mode

"""""""""""""""""""""""
"  Plugin Management  "
"""""""""""""""""""""""

" Vim-Plug:
call plug#begin('~/.vim/plugged')
"" tpope
Plug 'tpope/vim-repeat'                        " allows repeat '.' use with plugins
Plug 'tpope/vim-unimpaired'                    " quick navigation mappings
Plug 'tpope/vim-dispatch'                      " run make/build commands in background
Plug 'tpope/vim-fugitive'                      " git integration -- cheatsheet at https://gist.github.com/mikaelz/38600d22b716b39b031165cd6d201a67
Plug 'tpope/vim-surround'                      " modify quotes etc -- cheatsheet at https://cheatography.com/mutanclan/cheat-sheets/vim-tpope-vim-surround/

"" neoclide / coc set
Plug 'neoclide/coc.nvim', { 'branch': 'release' } " tab completion
Plug 'neoclide/coc-snippets', { 'do': 'yarn install --frozen-lockfile' }
Plug 'neoclide/coc-prettier', { 'do': 'yarn install --frozen-lockfile' } 
Plug 'neoclide/coc-yaml', { 'do': 'yarn install --frozen-lockfile' }
Plug 'neoclide/coc-eslint', { 'do': 'yarn install --frozen-lockfile' }
Plug 'neoclide/coc-lists', { 'do': 'yarn install --frozen-lockfile' }
Plug 'neoclide/coc-tsserver', { 'do': 'yarn install --frozen-lockfile' }
Plug 'neoclide/coc-stylelint', { 'do': 'yarn install --frozen-lockfile' }
Plug 'neoclide/coc-python', { 'do': 'yarn install --frozen-lockfile' }

"" colorschemes
Plug 'jacoborus/tender.vim'

"" snippet sources
Plug 'honza/vim-snippets' " large selection of snippets

"" functionality
Plug 'junegunn/vim-easy-align'                 " alignment plugin -- cheatsheet at https://devhints.io/vim-easyalign
Plug 'preservim/nerdcommenter'                 " commenting code
Plug 'preservim/nerdtree'                      " tree file explorer -- cheatsheet at https://cheatography.com/stepk/cheat-sheets/vim-nerdtree/
" Plug 'SirVer/ultisnips'                        " snippets
Plug 'ctrlpvim/ctrlp.vim'                      " fuzzy file finder
Plug 'christoomey/vim-tmux-navigator'          " seamless tmux and vim pane navigation
Plug 'myusuf3/numbers.vim'                     " <F5> to toggle relative and absolute line numbers, <F6> turn numbers on/off
Plug 'terryma/vim-multiple-cursors'            " multiple cursors
Plug 'easymotion/vim-easymotion'               " faster navigation
Plug 'jeetsukumaran/vim-buffergator'           " buffer navigation
Plug 'MattesGroeger/vim-bookmarks'             " toggle bookmarks by line -- mm, mn, mp
Plug 'triglav/vim-visual-increment'            " increment multiple locations with visual selection
Plug 'vifm/vifm.vim'                               " file manager

"" integrations
Plug 'jremmen/vim-ripgrep'                     " ripgrep integration -- :Rg <string|pattern>
Plug 'vim-scripts/sudo.vim'                    " open file with sudo -- :e sudo:/etc/passwd
Plug 'Xuyuanp/nerdtree-git-plugin'             " git icons in nerdtree
Plug 'jmcantrell/vim-virtualenv'               " python venv integration
Plug 'wincent/terminus'                        " better terminal integration, e.g. mouse support
Plug 'editorconfig/editorconfig-vim'           " reads and lints with editorconfig file
Plug 'roxma/vim-tmux-clipboard'                " unify clipboard with tmux

"" visuals
Plug 'ryanoasis/vim-devicons'                  " icons in vim
Plug 'tiagofumo/vim-nerdtree-syntax-highlight' " language icons in nerdtree
Plug 'vim-airline/vim-airline'                 " better status bar
Plug 'vim-airline/vim-airline-themes'          " better status bar - themes
Plug 'birnam/vim-indent-rainbow'               " colorful indentation spaces

"" language specific help
Plug 'hail2u/vim-css3-syntax'                  " Add CSS3 syntax support to vim's built-in `syntax/css.vim`
Plug 'yuezk/vim-js'                            " better js syntax highlighting
Plug 'maxmellon/vim-jsx-pretty'                " better jsx/tsx highlighting
Plug 'HerringtonDarkholme/yats.vim'            " better typescript highlighting
Plug 'PProvost/vim-ps1'                        " powershell highlighting
Plug 'guns/xterm-color-table.vim'              " testing colors with :XtermColorTable

" Plug 'joonty/vdebug'
" Plug 'birnam/vim-makegreen'
" Plug 'klen/python-mode'                        " expanded python functionality

" Required:
call plug#end()

"""""""""""""""""
"  Main Config  "
"""""""""""""""""

filetype plugin indent on
syntax enable

"" colorscheme
colorscheme tender
let g:airline_theme='tender'
let NERDSpaceDelims = 1
" color customizations
hi Folded               cterm=none ctermbg=237      ctermfg=38      gui=none        guibg=#3B3B3B   guifg=#90AB41

let g:airline_powerline_fonts = 1
let g:virtualenv_directory='~/virtualenv'

"" status line
"set status line for always (1=only with split, 2=always)
set laststatus=2
set statusline=%<%n:\ %F\ \%1*%m%*\ \ \%3*%h%w%2*%r%*%=%-20.(Line:\ %4*%l%*/%L\ \ Col:\ %c%V%)\ \ \%=%-40([0x%B]\ %Y%)\%P
if has("statusline") && has ("multi_byte")
	set statusline=%<%n:\ %F\ \%1*%m%*\ \ \%3*%h%w%2*%r%*%=%-20.(Line:\ %4*%l%*/%L\ \ Col:\ %c%V%)\ \ \%=%-40([U+%04B]\ %Y%)\%P
endif
hi User1 term=inverse,bold cterm=inverse,bold ctermfg=red ctermbg=white
hi User2 term=inverse,bold cterm=inverse,bold ctermfg=yellow ctermbg=white
hi User3 term=inverse,bold cterm=inverse,bold ctermfg=green ctermbg=white
hi User4 term=bold cterm=bold ctermfg=white


"" ctrl-/ shortcuts to comment toggle
nmap <C-_> <Plug>NERDCommenterToggle
nmap <leader>c<space> <Plug>NERDCommenterToggle
vmap <C-_> <Plug>NERDCommenterToggle<CR>

call togglerb#map("<F12>")
" let g:rainbow_colors_black= [ 234, 235, 236, 237, 238, 239 ] 
" let g:rainbow_colors_black= [ 183, 182, 181, 180, 179, 178, 159, 158, 157, 156, 155, 154 ] 
" let g:rainbow_colors_black= [ '#FF0000', '#00FF00', '#0000FF', '#FFFF00', '#FF00FF', '#00FFFF' ]
" let g:rainbow_colors_gui= [ "#FF0000", "#00FF00", "#0000FF", "#FFFF00", "#FF00FF", "#00FFFF" ]
" let g:rainbow_colors_gui= [ "#3C2C2C", "#3C342C", "#3C3C2C", "#343C2C", "#2C3C2C", "#2C3C34", "#2C3C3C", "#2C343C", "#2C2C3C", "#342C3C", "#3C2C3C", "#3C2C34" ]
let g:rainbow_colors_gui= [ "#3C2C2C", "#50463B", "#3C3C2C", "#46503B", "#2C3C2C", "#3B5046", "#2C3C3C", "#3B4650", "#2C2C3C", "#463B50", "#3C2C3C", "#503B46" ]
" let g:rainbow_colors_color= [ 226, 192, 195, 189, 225, 221 ]

""""""""""""""""""
"  coc settings  "
""""""""""""""""""
"" settings
" Give more space for displaying messages.
set cmdheight=2
" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300
" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

"" mappings
" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
" function! s:check_back_space() abort
  " let col = col('.') - 1
  " return !col || getline('.')[col - 1]  =~# '\s'
" endfunction

" inoremap <silent><expr> <TAB>
      " \ pumvisible() ? "\<C-n>" :
      " \ <SID>check_back_space() ? "\<TAB>" :
      " \ coc#refresh()
" inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" use <tab> for trigger completion and navigate to the next complete item
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of LS, ex: coc-tsserver
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
" }

" syntastic {
    " let g:syntastic_javascript_checkers = ['eslint'] " , 'jshint']
    " let g:syntastic_always_populate_loc_list = 1
" }

""""""""""""""""""""""""""
"  Plugin Configuration  "
""""""""""""""""""""""""""

"" UltiSnips
" let g:UltiSnipsExpandTrigger='<C-S-]>'
" let g:UltiSnipsJumpForwardTrigger='<TAB>'
" let g:UltiSnipsJumpBackwardTrigger='<S-TAB>'
" let g:coc_snippet_next = '<TAB>'
" let g:coc_snippet_prev = '<S-TAB>'

"" vim-unimpaired
    nmap <silent> [d :tabprev<cr>
    nmap <silent> ]d :tabnext<cr>
    nmap <silent> [i <C-x>
    nmap <silent> ]i <C-a>
    nmap <silent> [g :BookmarkNext<cr>
    nmap <silent> ]g :BookmarkPrev<cr>

    function! UnimpairedExtWinResize(direction, count)
	if a:direction == 'h<'
	    execute "vertical resize -" . a:count
	    silent! call repeat#set("\<Plug>UnimpairedExtWinResizeHSmall", a:count)
	elseif a:direction == 'h>'
	    execute "vertical resize +" . a:count
	    silent! call repeat#set("\<Plug>UnimpairedExtWinResizeHLarge", a:count)
	elseif a:direction == 'v<'
	    execute "resize -" . a:count
	    silent! call repeat#set("\<Plug>UnimpairedExtWinResizeVSmall", a:count)
	elseif a:direction == 'v>'
	    execute "resize +" . a:count
	    silent! call repeat#set("\<Plug>UnimpairedExtWinResizeVLarge", a:count)
	endif
    endfunction

    nmap <silent> <Plug>UnimpairedExtWinResizeVSmall :<C-U>call UnimpairedExtWinResize('v<', v:count1)<cr>
    nmap <silent> <Plug>UnimpairedExtWinResizeVLarge :<C-U>call UnimpairedExtWinResize('v>', v:count1)<cr>
    nmap <silent> <Plug>UnimpairedExtWinResizeHSmall :<C-U>call UnimpairedExtWinResize('h<', v:count1)<cr>
    nmap <silent> <Plug>UnimpairedExtWinResizeHLarge :<C-U>call UnimpairedExtWinResize('h>', v:count1)<cr>
    nmap [w <Plug>UnimpairedExtWinResizeVSmall
    nmap ]w <Plug>UnimpairedExtWinResizeVLarge
    nmap [W <Plug>UnimpairedExtWinResizeHSmall
    nmap ]W <Plug>UnimpairedExtWinResizeHLarge

"" folding
    set foldenable
    set foldmethod=syntax
    set foldlevelstart=99  " all open!

"" vim-bookmarks
	let g:bookmark_auto_save = 1
	let g:bookmark_manage_per_buffer = 1
	let g:bookmark_no_default_key_mappings = 1
	function! BookmarkMapKeys()
		nmap mm :BookmarkToggle<CR>
		nmap mi :BookmarkAnnotate<CR>
		nmap mn :BookmarkNext<CR>
		nmap mp :BookmarkPrev<CR>
		nmap ma :BookmarkShowAll<CR>
		nmap mc :BookmarkClear<CR>
		nmap mx :BookmarkClearAll<CR>
	endfunction
	function! BookmarkUnmapKeys()
	  unmap mm
	  unmap mi
	  unmap mn
	  unmap mp
	  unmap ma
	  unmap mc
	  unmap mx
	endfunction
	autocmd BufEnter * :call BookmarkMapKeys()
	autocmd BufEnter NERD_tree_* :call BookmarkUnmapKeys()

"" vim-easy-align
    vmap <Enter> <Plug>(EasyAlign)
    nmap ga <Plug>(EasyAlign)

"" vim-visual-increment
    vmap <leader>ii <Plug>VisualIncrement
    vmap <leader>ix <Plug>VisualDecrement

"" clipboard
" map <silent> <leader>y "+y
" vmap <silent> <leader>y "+y
xnoremap <silent> <leader>p "_dP
" map <silent> <leader>P "+P
" vmap <silent> <leader>p "+p
" vmap <silent> <leader>P "+P

"" vim-ctrlp
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

"" numbers.vim
nnoremap <F5> :NumbersToggle<CR>
" nnoremap <F6> :NumbersOnOff<CR>

"" buffergator
nnoremap <F8> :BuffergatorToggle<CR>
nnoremap <F9> :BuffergatorTabsToggle<CR>

"" ag
if executable('ag')
    let g:ackprg = 'ag --vimgrep'
endif

"" ?
" set timeoutlen=500 " this prevents delays when O is used for new-line-above
let g:vdebug_keymap = {
	    \"eval_visual" : "<Leader>vv"
	    \}

let g:showmarks_enable=0

"" tmux style split commands
map <silent> <C-w>- :sp<cr>
map <silent> <C-w>_ :sp<cr>
map <silent> <C-w>\ :vsp<cr>
map <silent> <C-w>\| :vsp<cr>

"" quickly edit/reload vimrc file -- from Derek Wyatt
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

"" nerdtree
map <silent> <F4> :NERDTreeToggle<cr>
map <silent> <F6> :NERDTreeFind<cr>
:let g:NERDTreeWinSize=40


"" ripgrep
let g:rg_command='rg --vimgrep --smart-case --follow'

" map <C-S-h> :tabnext<cr>
" map <C-S-l> :tabprev<cr>
" map <s-M-Right> :bnext<cr>
" map <s-M-Left> :bprev<cr>

"" terminus
let g:TerminusBracketedPaste=0


"""""""""""""""""""""""""
"  My custom shortcuts  "
"""""""""""""""""""""""""

nnoremap <silent> <C-o> :nohls<CR>
nnoremap <m-i> <C-a>
nnoremap <m-u> <C-x>

map <silent> <F3> gD
noremap <M-d> yyp
noremap <M-D> dd
map <silent> <F10> :set wrap!<cr>
map <silent> <C-t> <Esc>:tabe<cr>

" imap <C-space> <C-x><C-o>

nmap <silent> ,t :ToggleWord<cr>

nnoremap <C-f> :Rg<space>

"copy/paste
"nmap <C-V> "+gP
"imap <C-V> <ESC><C-V>i
"vmap <C-C> "+y
"vmap <C-V> d<Esc><C-V><Esc>


"""""""""""""""""
"  au commands  "
"""""""""""""""""

let g:ycm_autoclose_preview_window_after_insertion = 1

" au BufNewFile,BufRead *.as		setf actionscript
" au BufNewFile,BufRead *.mxml		setf mxml
"

au BufNewFile,BufRead Dockerfile.*	set ft=dockerfile
au BufNewFile,BufRead *.zsh-theme       set ft=zsh
au BufNewFile,BufRead *.zsh-theme	AirlineToggleWhitespace

"" category folding
function! VimFolds(lnum)
  let s:thisline = getline(a:lnum)
  if match(s:thisline, '^"" ') >= 0
    return '>2'
  endif
  if match(s:thisline, '^""" ') >= 0
    return '>3'
  endif
  let s:two_following_lines = 0
  if line(a:lnum) + 2 <= line('$')
    let s:line_1_after = getline(a:lnum+1)
    let s:line_2_after = getline(a:lnum+2)
    let s:two_following_lines = 1
  endif
  if !s:two_following_lines
      return '='
    endif
  else
    if (match(s:thisline, '^"""""') >= 0) &&
       \ (match(s:line_1_after, '^"  ') >= 0) &&
       \ (match(s:line_2_after, '^""""') >= 0)
      return '>1'
    else
      return '='
    endif
  endif
endfunction

""" defines a foldtext
function! VimFoldText()
  " handle special case of normal comment first
  let s:info = '('.string(v:foldend-v:foldstart).' l)'
  if v:foldlevel == 1
    let s:line = ' ◇ '.getline(v:foldstart+1)[3:-2]
  elseif v:foldlevel == 2
    let s:line = '   ●  '.getline(v:foldstart)[3:]
  elseif v:foldlevel == 3
    let s:line = '     ▪ '.getline(v:foldstart)[4:]
  endif
  if strwidth(s:line) > 80 - len(s:info) - 3
    return s:line[:79-len(s:info)-3+len(s:line)-strwidth(s:line)].'...'.s:info
  else
    return s:line.repeat(' ', 80 - strwidth(s:line) - len(s:info)).s:info
  endif
endfunction

""" set foldsettings automatically for vim files
augroup fold_vimrc
  autocmd!
  autocmd FileType vim 
                   \ setlocal foldmethod=expr |
                   \ setlocal foldexpr=VimFolds(v:lnum) |
                   \ setlocal foldtext=VimFoldText() |
     "              \ set foldcolumn=2 foldminlines=2
augroup END

""" keep folds in memory
autocmd BufWrite *.* mkview
autocmd BufRead *.* silent loadview
autocmd BufWrite .* mkview
autocmd BufRead .* silent loadview
