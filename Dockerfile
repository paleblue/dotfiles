# just an example to show dependencies
# NOTE: you'll have to start nvim several times to complete the plugin installations, but it should be automatic
# VIM might require either :PlugInstall and/or :CocUpdate once or twice

FROM ubuntu:jammy as umain

# USER root
RUN mkdir -p /root/dotfiles
WORKDIR /root/dotfiles

COPY . .

WORKDIR /root

RUN apt-get update

FROM umain as uupdated
RUN apt-get install -y locales zsh curl wget git tmux build-essential libreadline-dev unzip python3 python3-pip ripgrep highlight vifm

RUN locale-gen en_US.UTF-8
RUN chsh -s /usr/bin/zsh
ENV SHELL=/usr/bin/zsh 
ENV NVM_DIR=/root/.nvm

RUN wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash && \
	[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && \
	[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" && \
	nvm install v16 && \
	npm install -g npm@latest && \
	npm install -g prettier eslint eslint_d tree-sitter tree-sitter-cli @fsouza/prettierd neovim

RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" && \
	git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

RUN curl -R -O http://www.lua.org/ftp/lua-5.3.5.tar.gz && \
	tar -zxf lua-5.3.5.tar.gz && \
	cd lua-5.3.5 && \
	make linux test && \
	make install && \
	cd /root && \
	wget https://luarocks.org/releases/luarocks-3.8.0.tar.gz && \
	tar zxpf luarocks-3.8.0.tar.gz && \
	cd luarocks-3.8.0 && \
	./configure --with-lua-include=/usr/local/include && \
	make && \
	make install && \
	cd /root && \
	pip3 install neovim && \
	wget https://github.com/sharkdp/fd/releases/download/v8.6.0/fd-musl_8.6.0_amd64.deb && \
	dpkg -i fd-musl_8.6.0_amd64.deb 

RUN wget https://github.com/JohnnyMorganz/StyLua/releases/download/v0.15.3/stylua-linux-x86_64.zip && \
	unzip stylua-linux-x86_64.zip && \
	mv stylua /usr/bin/

RUN wget https://github.com/neovim/neovim/releases/download/nightly/nvim-linux64.deb && \
	dpkg -i ./nvim-linux64.deb

RUN wget http://ftp.us.debian.org/debian/pool/main/v/vim/vim_9.0.1000-4_amd64.deb && \
	wget http://ftp.us.debian.org/debian/pool/main/v/vim/vim-runtime_9.0.1000-4_all.deb && \
	wget http://ftp.us.debian.org/debian/pool/main/v/vim/vim-common_9.0.1000-4_all.deb && \
	apt install -y --fix-broken ./vim-common_9.0.1000-4_all.deb ./vim-runtime_9.0.1000-4_all.deb ./vim_9.0.1000-4_amd64.deb && \
	curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
	    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

RUN wget https://github.com/jesseduffield/lazygit/releases/download/v0.37.0/lazygit_0.37.0_Linux_x86_64.tar.gz &&\
	tar -xvf lazygit_0.37.0_Linux_x86_64.tar.gz && \
	mv lazygit /usr/local/bin

RUN rm *.deb && rm *.zip && rm *.tar.gz && rm -rf lua-5.3.5 && rm -rf luarocks-3.8.0

FROM uupdated as udotfiles
RUN mkdir -p /root/.oh-my-zsh/themes && \
	mkdir -p /root/.oh-my-zsh/custom
RUN rm -f /root/.zshrc
RUN ln -s /root/dotfiles/zsh/.zshrc /root/ && \
	ln -s /root/dotfiles/zsh/zshcommon.zsh /root/ && \
	ln -s /root/dotfiles/zsh/.oh-my-zsh/themes/*.zsh-theme /root/.oh-my-zsh/custom/themes/ && \
	ln -s /root/dotfiles/zsh/.oh-my-zsh/custom/dircolors.256dark /root/.oh-my-zsh/custom/

RUN mkdir /root/.config \
	ln -s /root/dotfiles/nvim /root/.config/ && \
	ln -s /root/dotfiles/tmux/.tmux.conf* /root/ && \
	ln -s /root/dotfiles/vim/.vimrc /root/ && \
	ln -s /root/dotfiles/vifm /root/.config/ && \
	ln -s /root/dotfiles/utilities/* /usr/local/bin/

CMD /bin/zsh
