-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ "n", "v" }, "<Space>", "<Nop>", { silent = true })

-- Remap for dealing with word wrap
-- vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
-- vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- vim.keymap.set("n", "<F5>", vim.cmd.NumbersToggle, { noremap = true })
-- vim.keymap.set("n", "<F6>", vim.cmd.NumbersOnOff, { noremap = true })

vim.keymap.set("n", "[d", vim.cmd.tabprev)
vim.keymap.set("n", "]d", vim.cmd.tabnext)
vim.keymap.set("n", "[i", "<C-x>")
vim.keymap.set("n", "]i", "<C-a>")
vim.keymap.set("n", "[g", vim.cmd.BookmarkPrev)
vim.keymap.set("n", "]g", vim.cmd.BookmarkNext)
vim.keymap.set("n", "[r", vim.diagnostic.goto_prev)
vim.keymap.set("n", "]r", vim.diagnostic.goto_next)

vim.keymap.set("x", "<Enter>", "<Plug>(EasyAlign)")
vim.keymap.set("x", "ga", "<Plug>(EasyAlign)")
vim.keymap.set("n", "<C-o>", vim.cmd.nohls)
vim.keymap.set("n", "<F10>", ":set wrap!<CR>")

vim.keymap.set("n", "<F4>", vim.cmd.NvimTreeToggle)
vim.keymap.set("n", "<leader><F4>", vim.cmd.NvimTreeFindFileToggle)

vim.keymap.set("x", "<leader>p", '"_dP')
-- vim.keymap.set('n', '<leader>y', '"+y')

-- vim.keymap.set("n", "<leader>mp", vim.cmd.MarkdownPreview)

vim.keymap.set("n", "<leader>gg", vim.cmd.LazyGit)

vim.keymap.set("n", "<leader>rr", require("telescope.builtin").resume, { desc = "[r]estore previous Telescope"})

-- See `:help telescope.builtin`
vim.keymap.set("n", "<leader>?", require("telescope.builtin").oldfiles, { desc = "[?] Find recently opened files" })
vim.keymap.set("n", "<leader><space>", require("telescope.builtin").buffers, { desc = "[ ] Find existing buffers" })
vim.keymap.set("n", "<leader>/", function()
  -- You can pass additional configuration to telescope to change theme, layout, etc.
  require("telescope.builtin").current_buffer_fuzzy_find(require("telescope.themes").get_dropdown({
    winblend = 10,
    previewer = false,
  }))
end, { desc = "[/] Fuzzily search in current buffer]" })

local tele = require("telescope.builtin")
vim.keymap.set("n", "<leader>sf", tele.find_files, { desc = "[S]earch [F]iles" })
vim.keymap.set("n", "<C-p>", tele.git_files, { desc = "[S]earch [F]iles" })
vim.keymap.set("n", "<leader>sh", tele.help_tags, { desc = "[S]earch [H]elp" })
vim.keymap.set("n", "<leader>sw", tele.grep_string, { desc = "[S]earch current [W]ord" })
vim.keymap.set("n", "<leader>sg", tele.live_grep, { desc = "[S]earch by [G]rep" })
vim.keymap.set("n", "<leader>sd", tele.diagnostics, { desc = "[S]earch [D]iagnostics" })
vim.keymap.set(
  "n",
  "<leader>sb",
  require("telescope").extensions.vim_bookmarks.current_file,
  { desc = "[S]earch [B]ookmarks (current file)" }
)
vim.keymap.set(
  "n",
  "<leader>sB",
  require("telescope").extensions.vim_bookmarks.all,
  { desc = "[S]earch [B]ookmarks (all files)" }
)

-- tmux style split commands
vim.keymap.set("n", "<C-w>-", ":sp<cr>")
vim.keymap.set("n", "<C-w>_", ":sp<cr>")
vim.keymap.set("n", "<C-w>\\", ":vsp<cr>")
vim.keymap.set("n", "<C-w>\\|", ":vsp<cr>")

-- vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float)
vim.keymap.set("n", "<leader>e", vim.lsp.buf.code_action)
vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist)

vim.keymap.set("n", "<leader>Q", ":bufdo bwipeout<cr>")

