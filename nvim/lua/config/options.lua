-- [[ Setting options ]]
-- See `:help vim.o`

-- new tab by default
vim.o.switchbuf = vim.o.switchbuf .. ',usetab,newtab'

-- disable netrw to use nvim-tree instead
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- Set highlight on search
vim.o.hlsearch = true

-- highlight current line
vim.o.cursorline = true

-- Make line numbers default
vim.wo.number = true

-- Enable mouse mode
vim.o.mouse = "a"

-- Enable break indent
vim.o.breakindent = true

-- Save undo history
vim.o.undofile = true

-- Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.autoindent = true
vim.o.smartindent = true

vim.o.tabstop = 2
vim.o.softtabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = true

-- Decrease update time
vim.o.updatetime = 250
vim.wo.signcolumn = "yes"

-- Set colorscheme
vim.o.termguicolors = true

-- Set completeopt to have a better completion experience
vim.o.completeopt = "menu,menuone,noselect"

-- [[ Basic Keymaps ]]
-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.o.splitbelow = true
vim.o.splitright = true

-- having tmux handle clipboard is ok -- but copying text in windows does not update its paste buffers
if vim.fn.has("tmux") and vim.fn.executable("tmux") then
	vim.g.clipboard = {
		name = "wsl+tmux",
		cache_enabled = true,
		copy = {
			["+"] = { "tmux", "load-buffer", "-" },
			["*"] = { "tmux", "load-buffer", "-" },
		},
		paste = {
			["+"] = { "tmux", "save-buffer", "-" },
			["*"] = { "tmux", "save-buffer", "-" },
		},
	}
	-- else
	-- if vim.fn.executable("win32yank.exe") then
	-- 	vim.g.clipboard = {
	-- 		name = "wsl",
	-- 		cache_enabled = true,
	-- 		copy = {
	-- 			["+"] = { "win32yank.exe", "-i", "--crlf" },
	-- 			["*"] = { "win32yank.exe", "-i", "--crlf" },
	-- 		},
	-- 		paste = {
	-- 			["+"] = { "win32yank.exe", "-o", "--lf" },
	-- 			["*"] = { "win32yank.exe", "-o", "--lf" },
	-- 		},
	-- 	}
end

-- keep numbers out of things
vim.g.numbers_exclude = {
	"unite",
	"tagbar",
	"startify",
	"gundo",
	"vimshell",
	"w3m",
	"minibufexpl",
	"nerdtree",
	"nvimtree",
	"NvimTree",
	"neo-tree",
}

vim.g.mkdp_filetypes = { "markdown" }

-- if vim.fn.has("wsl") == 1 then
-- 	vim.api.nvim_create_autocmd("TextYankPost", {
-- 		group = vim.api.nvim_create_augroup("Yank", { clear = true }),
-- 		callback = function()
-- 			vim.fn.system("clip.exe", vim.fn.getreg('"'))
-- 		end,
-- 	})
-- end

vim.o.clipboard = "unnamedplus"

-- new recommentations from EthanWng97
vim.opt.sessionoptions = "buffers,curdir,folds,help,tabpages,terminal,globals"
vim.opt.swapfile = false
vim.opt.updatetime = 100
vim.opt.spelloptions = "camel,noplainbuffer"
vim.opt.foldlevel = 99
vim.o.foldcolumn = "1"
-- vim.o.foldlevelstart = 99
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
vim.opt.foldenable = true
vim.opt.fillchars = {
	foldopen = "",
	foldclose = "",
	fold = " ",
	foldsep = " ",
	diff = "/",
	eob = " ",
}
vim.opt.mousemoveevent = true

-- command completion
vim.opt.wildmode = "longest:full:full"
vim.opt.wildignore = "*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx,*DS_STORE,*.db"
