local M = {
  "akinsho/bufferline.nvim",
  event = "VeryLazy",
  keys = {
    { "<leader>bp", "<cmd>BufferLineTogglePin<cr>" },
  },
  opts = {
    options = {
      diagnostics = "nvim_lsp",
      mode = "tabs",
      indicator = {
        icon = '▌',
        style = 'icon',
      },
      hover = {
        enabled = true,
        delay = 0,
        reveal = { "close" },
      },
    },
  },
}

return M
