local M = {
  "lukas-reineke/indent-blankline.nvim",
  main = "ibl",
  event = "BufReadPre",
  config = function()
    local highlight = {
        "RainbowRed",
        "RainbowYellow",
        "RainbowBlue",
        "RainbowOrange",
        "RainbowGreen",
        "RainbowViolet",
        "RainbowCyan",
    }
    local highlightDim = {
        "DimRainbowRed",
        "DimRainbowYellow",
        "DimRainbowBlue",
        "DimRainbowOrange",
        "DimRainbowGreen",
        "DimRainbowViolet",
        "DimRainbowCyan",
    }
    local hooks = require "ibl.hooks"
    -- create the highlight groups in the highlight setup hook, so they are reset
    -- every time the colorscheme changes
    hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
        vim.api.nvim_set_hl(0, "DimRainbowRed", { fg = "#3F3132" })
        vim.api.nvim_set_hl(0, "DimRainbowYellow", { fg = "#403A30" })
        vim.api.nvim_set_hl(0, "DimRainbowBlue", { fg = "#32393E" })
        vim.api.nvim_set_hl(0, "DimRainbowOrange", { fg = "#3A332C" })
        vim.api.nvim_set_hl(0, "DimRainbowGreen", { fg = "#32382E" })
        vim.api.nvim_set_hl(0, "DimRainbowViolet", { fg = "#373239" })
        vim.api.nvim_set_hl(0, "DimRainbowCyan", { fg = "#243133" })
    end)
    hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
        vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#AD7176" })
        vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#AE986F" })
        vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#7491AA" })
        vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#A9896A" })
        vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#85A56F" })
        vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#9875A3" })
        vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#5BA0A9" })
    end)

    vim.g.rainbow_delimiters = { highlight = highlight }
    require("ibl").setup { scope = { highlight = highlightDim } }

    hooks.register(hooks.type.SCOPE_HIGHLIGHT, hooks.builtin.scope_highlight_from_extmark)

    require("ibl").setup {
      indent = {
        -- char = "▏",
        char = "▎",
        highlight = highlightDim,
      },
      scope = {
        enabled = true,
        show_end = true,
        show_exact_scope = false, -- experiment with
      },
      exclude = {
        filetypes = {
          "lspinfo",
          "checkhealth",
          "gitcommit",
          "TelescopePrompt",
          "TelescopeResults",
          "''",
          "coc-explorer",
          "dashboard",
          "floaterm",
          "alpha",
          "help",
          "packer",
          "NvimTree",
        },
        buftypes = {
          "terminal",
          "nofile",
          "quickfix",
          "prompt",
        },
      },
    }
  end,
}

return M
