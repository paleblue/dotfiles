local M = {
    "SmiteshP/nvim-navbuddy",
    lazy = false,
    dependencies = {
        "neovim/nvim-lspconfig",
        "SmiteshP/nvim-navic",
        "MunifTanjim/nui.nvim"
    },
    keys = {
          { "<leader><Enter>", "<cmd>Navbuddy<cr>", desc = "Nav" },
    },
    config = function ()
        local actions = require("nvim-navbuddy.actions")
        local navbuddy = require("nvim-navbuddy")
        navbuddy.setup({
            window = {
                border = "double"
            },
            lsp = { auto_attach = true }
        })
    end
}

return M
