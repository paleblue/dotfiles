local M = {
  "nvim-tree/nvim-tree.lua",
  version = "*",
  lazy = false,
  dependencies = {
    "nvim-tree/nvim-web-devicons",
  },
config = function()
	require("nvim-tree").setup ({
		sort = {
        sorter = "case_sensitive",
    },
		view = {
			adaptive_size = true,
			relativenumber = false,
		},
		renderer = {
			group_empty = true,
		},
		filters = {
			dotfiles = true,
		},
    on_attach = function (bufnr)
        local api = require("nvim-tree.api")

        local function opts(desc)
          return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true}
        end

        -- default mappings
        api.config.mappings.default_on_attach(bufnr)

        -- custom mappings
        vim.keymap.set('n', 'u', api.tree.change_root_to_parent, opts('Up'))
        vim.keymap.set('n', 'i', api.node.open.horizontal, opts('Open: Horizontal Split'))
        vim.keymap.set('n', 's', api.node.open.vertical, opts('Open: Vertical Split'))
        vim.keymap.set('n', 't', api.node.open.tab, opts('Open: New Tab'))
        vim.keymap.set('n', '?', api.tree.toggle_help, opts('Help'))
        vim.keymap.set('n', '<C-c>', api.tree.change_root_to_node, opts('CD'))
    end,
	})
end,
}

return M
