return {
  -- { "nvim-lua/plenary.nvim" },
  { "nvim-tree/nvim-web-devicons", opts = { default = true }, lazy = false },
  { "j-hui/fidget.nvim", lazy = false },
  --
  -- Git related plugins
  { "tpope/vim-fugitive", lazy = false },
  -- { "tpope/vim-rhubarb" },

  { "tpope/vim-sleuth", lazy = false },                   -- Detect tabstop and shiftwidth automatically
  { "tom-anders/telescope-vim-bookmarks.nvim", lazy = false },

  { "tpope/vim-unimpaired", lazy = false },        -- quick navigation mappings
  { "tpope/vim-surround", lazy = false },          -- modify quotes etc -- cheatsheet at https://cheatography.com/mutanclan/cheat-sheets/vim-tpope-vim-surround/
  { "myusuf3/numbers.vim", lazy = false, keys = {
    { "<F5>", "<cmd>NumbersToggle<cr>", desc = "Toggle relative line numbers" },
    { "<F6>", "<cmd>NumbersOnOff<cr>", desc = "Toggle line number visibility" },
  }},         -- <F5> to toggle relative and absolute line numbers, <F6> turn numbers on/off
  -- { "gpanders/editorconfig.nvim", lazy = false },  -- reads and lints with editorconfig file
  { "junegunn/vim-easy-align", lazy = false },     -- alignment plugin -- cheatsheet at https://devhints.io/vim-easyalign
  { "MattesGroeger/vim-bookmarks", lazy = false }, -- toggle bookmarks by line -- mm, mn, mp
  { "mg979/vim-visual-multi", lazy = false },      -- multiple cursors
  { "triglav/vim-visual-increment", lazy = false }, -- increment multiple locations with visual selection

  { "MunifTanjim/nui.nvim" },

  -- Utilities
  -- {
  -- 	"folke/persistence.nvim",
  -- 	lazy = false,
  -- 	keys = {
  -- 		{
  -- 			"<leader>ls",
  -- 			function()
  -- 				require("persistence").load()
  -- 			end,
  -- 		},
  -- 	},
  -- 	opts = { options = { "buffers", "curdir", "folds", "help", "tabpages", "terminal", "globals" } },
  -- },
  -- {
  -- "rmagatti/auto-session",
  -- lazy = false,
  -- opts = {
  -- auto_session_suppress_dirs = { "~/Downloads" },
  -- },
  -- },
  -- {
  -- "jackMort/ChatGPT.nvim",
  -- cmd = { "ChatGPT", "ChatGPTActAs" },
  -- config = true,
  -- },
}
