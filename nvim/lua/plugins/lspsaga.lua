local M = {
  "nvimdev/lspsaga.nvim",
  version = "*",
  lazy = false,
  dependencies = {
    "nvim-treesitter/nvim-treesitter",
    "nvim-tree/nvim-web-devicons",
  },
config = function()
	require("lspsaga").setup ({
      finder = {
        keys = {
          shuttle = '<Tab>'
        }
      },
      definition = {
        keys = {
          vsplit = '<C-s>',
          split = '<C-i>',
        }
      },
      lightbulb = {
        enable = false,
      }
    })
end,
}

return M
