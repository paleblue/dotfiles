export MOUNTBASE=/mnt
export PATH=~/.local/bin:$PATH

export HISTSIZE=1000
export SAVEHIST=1000

# X config
# export DISPLAY=localhost:10.0
# export DISPLAY=host.docker.internal:0
# export DISPLAY=$(ip route show default | awk '{print $3}'):0.0
export LIBGL_ALWAYS_INDIRECT=1
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
export PYENV_ROOT=$MOUNTBASE/c/Program\ Files/pyenv

# export DOCKER_HOST=tcp://localhost:2375

# export BROWSER="/mnt/c/Program Files (x86)/Google/Chrome/Application/chrome.exe"

# eval `dircolors $HOME/.oh-my-zsh/custom/dircolors.256dark`
zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"
autoload -Uz compinit
compinit -C

# alias ls='ls --color=auto'
# alias lah='ls -lah'
alias ls='eza --color=always'
alias lah='eza --color=always --long --git'
alias tre='eza --color=always --tree'
alias tree='eza --color=always --long --git --tree -I "node_modules|.git"'
alias treee='eza --color=always --long --git --tree --level=2 -I "node_modules|.git"'
alias tree2='eza --color=always --long --git --tree --level=2 -I "node_modules|.git"'
alias treeee='eza --color=always --long --git --tree --level=3 -I "node_modules|.git"'
alias tree3='eza --color=always --long --git --tree --level=3 -I "node_modules|.git"'
alias treeeee='eza --color=always --long --git --tree --level=4 -I "node_modules|.git"'
alias tree4='eza --color=always --long --git --tree --level=4 -I "node_modules|.git"'
alias duh='du -h -d 1'

alias ggrep='grep -RniIs --exclude-dir node_modules --exclude-dir test --exclude-dir .git'
# alias wgit="$MOUNTBASE/c/Program\ Files/Git/bin/git.exe"

alias e='emacs25 -nw'

alias clr="cd ~ && clear"

# alias clip="$MOUNTBASE/c/Windows/System32/clip.exe"

alias rg="/usr/bin/rg --smart-case"

# alias tree="/usr/bin/tree -I 'node_modules|.git'"
# alias adbw="/c/Users/birna/AppData/Local/Android/Sdk/platform-tools/adb.exe"

alias eslint="/home/david/.nvm/versions/node/v20.11.0/bin/eslint_d"
alias nvimstartup="rm ~/nvim-startup.txt && nvim --startuptime ~/nvim-startup.txt ~/nvim-startup.txt"

alias fmcb="ADMIN_USER=admin ADMIN_PASSWORD=Forcepoint2021! APP_PORT=9090 SMTP_URL=172.25.129.26 MONGODB_URL=mongodb://mongo1/ npm run dev"
alias fmcbviz="ADMIN_USER=admin ADMIN_PASSWORD=Forcepoint2021! APP_PORT=9090 SMTP_URL=172.25.129.26 MONGODB_URL=mongodb://mongo1/ VIZ_URL=172.25.144.160 GRAFANA_PORT=8080 DASHBOARD_ID=bdqgw337d962oc ORD_ID=1 COLLECTD_PORT=8008 npm run dev"
alias fmcbquiet="ADMIN_USER=admin ADMIN_PASSWORD=Forcepoint2021! APP_PORT=9090 QUIET_PUBLISH_MESSAGE=true QUIET_API_MESSAGE=true SMTP_URL=172.25.129.26 MONGODB_URL=mongodb://mongo1/ npm run dev"
alias fmcrm="MONGODB_URL=mongodb://mongo1/fmc npm run start"
# alias fmcf="REACT_APP_SKIPREVIEWITEMSCHECK=skip REACT_APP_BUSINESSURL=http://172.25.144.205:3000 PORT=3001 npm start"
# alias fmcf="REACT_APP_SKIPREVIEWITEMSCHECK=skip REACT_APP_BUSINESSURL=http://172.25.144.205:9090 PORT=8080 npm start"
# alias fmcf="REACT_APP_SKIPREVIEWITEMSCHECK=skip REACT_APP_BUSINESSURL=http://172.25.144.205:9090 npm start -- --port 8080 --host"
alias fmcf="VITE_APP_SKIPREVIEWITEMSCHECK=skip VITE_APP_BUSINESSURL=http://172.25.144.205:9090 npm start -- --port 8080 --host"

alias fmcfrev="REACT_APP_BUSINESSURL=http://172.25.144.205:9090 PORT=8080 npm start"

# alias vpnip="ip addr show | grep 172.19.61 | awk '{ print \$2 }' | awk -F/ '{ print \$1 }'"
# alias fmcf2="REACT_APP_BUSINESSURL=http://\$(vpnip):3000 PORT=3001 npm start"

alias vpnip="ip addr show | grep 172.17.60 | awk '{ print \$2 }' | awk -F/ '{ print \$1 }'"
alias fmcf2="REACT_APP_BUSINESSURL=http://172.25.144.205:9090 PORT=8080 npm start"

alias mongoip="docker inspect \$(docker ps | grep mongo | awk '{ print \$1 }') | grep \\\"IPAddress | grep '[0-9]\+.*' | awk -F\\\" '{ print \$4 }'"

# alias cat="highlight $1 --out-format xterm256 -l -f -t 2 -s zenburn --force"
alias cat="batcat"
alias catt="/usr/bin/cat"

alias docker-kill-all="docker kill \$(docker ps -q)"
alias docker-prune="docker kill \$(docker ps -q) && docker system prune -af && docker volume prune -f"

export WORKON_HOME=$HOME/.virtualenv
# export PROJECT_HOME=/mnt/e/Projects/labs/py
export VIRTUALENV_PYTHON=/usr/bin/python3

export PATH=/usr/local/bin:$PATH
export PATH=$PYENV_ROOT/bin:$PATH
export PATH=$HOME/.rvm/bin:$PATH
export PATH=$HOME/.yarn/bin:$PATH
export PATH=/usr/local/go/bin:$PATH
unsetopt beep
