require("nvim-tree").setup({
	sort_by = "case_sensitive",
	view = {
		adaptive_size = true,
		relativenumber = false,
		mappings = {
			list = {
				{ key = "u",     action = "dir_up" },
				{ key = "i",     action = "split" },
				{ key = "s",     action = "vsplit" },
				{ key = "t",     action = "tabnew" },
				{ key = "?",     action = "toggle_help" },
				{ key = "<C-c>", action = "cd" },
			},
		},
	},
	renderer = {
		group_empty = true,
	},
	filters = {
		dotfiles = true,
	},
})

-- Set lualine as statusline
-- See `:help lualine.txt`
require("lualine").setup({
	options = {
		icons_enabled = false,
		theme = "onedark",
		component_separators = "|",
		section_separators = "",
	},
})

-- Enable Comment.nvim
require("Comment").setup()

-- Enable `lukas-reineke/indent-blankline.nvim`
-- See `:help indent_blankline.txt`
require("ibl").setup({
	indent = { highlight = { "Whitespace", "NonText" }, char = "┊" },
	whitespace = { highlight = { "Whitespace", "NonText" }, remove_blankline_trail = false },
	scope = { enabled = false },
})

-- Gitsigns
-- See `:help gitsigns.txt`
require("gitsigns").setup({
	signs = {
		add = { text = "+" },
		change = { text = "~" },
		delete = { text = "_" },
		topdelete = { text = "‾" },
		changedelete = { text = "~" },
	},
})

-- [[ Configure Telescope ]]
-- See `:help telescope` and `:help telescope.setup()`
require("telescope").setup({
	defaults = {
		mappings = {
			i = {
				["<C-u>"] = false,
				["<C-d>"] = false,
				["<C-i>"] = "select_horizontal",
				["<C-s>"] = "select_vertical",
			},
			n = {
				["i"] = "select_horizontal",
				["s"] = "select_vertical",
				["<C-i>"] = "select_horizontal",
				["<C-s>"] = "select_vertical",
			},
		},
	},
	extensions = {
		ctags_outline = {
			--ctags option
			ctags = { "ctags" },
			--ctags filetype option
			ft_opt = {
				vim = "--vim-kinds=fk",
				lua = "--lua-kinds=fk",
			},
		},
	},
})

require("telescope").load_extension("vim_bookmarks")

-- Enable telescope fzf native, if installed
pcall(require("telescope").load_extension, "fzf")

require("telescope").load_extension("ctags_outline")

-- Turn on lsp status information
require("fidget").setup()

-- enable autopairs
require("nvim-autopairs").setup()

-- Make runtime files discoverable to the server
local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

-- require("refactoring").setup()

require("paleblue.setups.treesitter")

require("paleblue.setups.cmp")

require("paleblue.setups.lsp")
