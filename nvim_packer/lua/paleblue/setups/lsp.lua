-- LSP settings.
local register_lsp_buffer_keymaps = function(_, bufnr)
	local nmap = function(keys, func, desc)
		if desc then
			desc = "LSP: " .. desc
		end

		vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
	end

	nmap("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
	nmap("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")

	nmap("gd", vim.lsp.buf.definition, "[G]oto [D]efinition")
	nmap("gr", require("telescope.builtin").lsp_references, "[G]oto [R]eferences")
	nmap("gI", vim.lsp.buf.implementation, "[G]oto [I]mplementation")
	nmap("<leader>D", vim.lsp.buf.type_definition, "Type [D]efinition")
	nmap("<leader>ds", require("telescope.builtin").lsp_document_symbols, "[D]ocument [S]ymbols")
	nmap("<leader>ws", require("telescope.builtin").lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")

	-- See `:help K` for why this keymap
	nmap("gk", vim.lsp.buf.hover, "Hover Documentation")
	-- nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')
	nmap("gK", vim.lsp.buf.signature_help, "Signature Documentation")

	-- Lesser used LSP functionality
	nmap("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
	nmap("<leader>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder")
	nmap("<leader>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder")
	nmap("<leader>wl", function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, "[W]orkspace [L]ist Folders")
end

local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

on_attach = function(client, bufnr)
	register_lsp_buffer_keymaps(client, bufnr)

	if client.supports_method("textDocument/formatting") then
		vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
		vim.api.nvim_create_autocmd("BufWritePre", {
			group = augroup,
			buffer = bufnr,
			callback = function()
				vim.lsp.buf.format()
			end,
		})
	end

	-- https://github.com/neovim/nvim-lspconfig/wiki/UI-Customization#customizing-how-diagnostics-are-displayed
	vim.diagnostic.config({
		virtual_text = {
			severity = vim.diagnostic.severity.ERROR,
		},
	})
	vim.api.nvim_create_autocmd("CursorHold", {
		buffer = bufnr,
		callback = function()
			local opts = {
				focusable = false,
				close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
				border = "rounded",
				source = "always",
				prefix = "■  ",
				scope = "cursor",
			}
			vim.diagnostic.open_float(nil, opts)
		end,
	})
end

local null_ls = require("null-ls")
null_ls.setup({
	sources = {
		null_ls.builtins.formatting.stylua,
		null_ls.builtins.formatting.eslint_d,
		null_ls.builtins.formatting.prettierd,
		null_ls.builtins.diagnostics.eslint,
		-- null_ls.builtins.completion.spell,
	},
	on_attach = on_attach,
})

-- Setup mason so it can manage external tooling
require("mason").setup()

-- Enable the following language servers
-- Feel free to add/remove any LSPs that you want here. They will automatically be installed
local servers = {
	"clangd",
	"rust_analyzer",
	"pyright",
	"tsserver",
	"lua_ls",
	"dockerls",
	"diagnosticls",
}

-- Ensure the servers above are installed
require("mason-lspconfig").setup({
	ensure_installed = servers,
})

require("mason-tool-installer").setup({
	ensure_installed = {
		"prettierd",
	},
})

-- nvim-cmp supports additional completion capabilities
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)
capabilities.offsetEncoding = "utf-8"

require("mason-lspconfig").setup_handlers({
	function(server_name)
		if server_name == "tsserver" then
			server_name = "ts_ls"
		end
		require("lspconfig")[server_name].setup({
			capabilities = capabilities,
		})
	end,

	["lua_ls"] = function()
		require("lspconfig").lua_ls.setup({
			settings = {
				Lua = {
					diagnostics = {
						globals = { "vim" },
					},
					workspace = {
						library = vim.api.nvim_get_runtime_file("", true),
						checkThirdParty = false,
					},
				},
			},
		})
	end,

	["clangd"] = function()
		require("lspconfig").clangd.setup({
			on_attach = on_attach,
			capabilities = capabilities,
			cmd = {
				"clangd",
				"--background-index",
				"--pch-storage=memory",
				"--clang-tidy",
				"--suggest-missing-includes",
				"--cross-file-rename",
				"--completion-style=detailed",
				"--query-driver=/usr/bin/gcc,/usr/bin/arm-none-eabi-gcc",
			},
			init_options = {
				clangdFileStatus = true,
				usePlaceholders = true,
				completeUnimported = true,
				semanticHighlighting = true,
			},
			filetypes = { "c", "cpp" },
		})
	end,

	["tsserver"] = function()
		require("lspconfig").tsserver.setup({
			on_attach = on_attach,
			capabilities = capabilities,
			filetypes = {
				"typescript",
				"typescriptreact",
				"typescript.tsx",
				"javascript",
				"javascriptreact",
				"javascript.jsx",
			},
			cmd = { "typescript-language-server", "--stdio" },
		})
	end,

	-- ["diagnosticls"] = function()
	-- 	require("lspconfig").diagnosticls.setup({
	-- 		on_attach = on_attach,
	-- 		filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact", "css" },
	-- 		init_options = {
	-- 			filetypes = {
	-- 				javascript = "eslint",
	-- 				typescript = "eslint",
	-- 				javascriptreact = "eslint",
	-- 				typescriptreact = "eslint",
	-- 			},
	-- 			linters = {
	-- 				eslint = {
	-- 					sourceName = "eslint_d",
	-- 					command = "/home/david/.nvm/versions/node/v16.13.2/bin/eslint_d",
	-- 					rootPatterns = {
	-- 						".eslitrc.js",
	-- 						"package.json",
	-- 					},
	-- 					debounce = 100,
	-- 					args = {
	-- 						"--cache",
	-- 						"--stdin",
	-- 						"--stdin-filename",
	-- 						"%filepath",
	-- 						"--format",
	-- 						"json",
	-- 					},
	-- 					parseJson = {
	-- 						errorsRoot = "[0].messages",
	-- 						line = "line",
	-- 						column = "column",
	-- 						endLine = "endLine",
	-- 						endColumn = "endColumn",
	-- 						message = "${message} [${ruleId}]",
	-- 						security = "severity",
	-- 					},
	-- 					securities = {
	-- 						[2] = "error",
	-- 						[1] = "warning",
	-- 					},
	-- 				},
	-- 			},
	-- 		},
	-- 	})
	-- end,
})
