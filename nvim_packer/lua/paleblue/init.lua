require("paleblue.packer")

require("paleblue.sets")

require("paleblue.setups")

require("paleblue.autocmds")

require("paleblue.keys")
